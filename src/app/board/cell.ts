export class Cell {
  status: 'open' | 'clear' | 'flag' = 'open';
  mine = false;
  surroundedMines = 0;

  constructor(public row: number, public column: number) {
  }
}
