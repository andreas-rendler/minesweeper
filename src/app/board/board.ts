import {Cell} from './cell';

const PEERS = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]];

export class Board {

  cells: Cell[][] = [];

  minesTotal = 0;

  constructor(size: number, mines: number) {

    // init board
    for (let y = 0; y < size; y++) {
      this.cells[y] = [];
      for (let x = 0; x < size; x++) {
        this.cells[y][x] = new Cell(y, x);
      }
    }

    // assign mines
    for (let i = 0; i <= mines; i++) {
      const x: number = this.getRandomInt(size);
      const y: number = this.getRandomInt(size);

      this.cells[x][y].mine = true;
    }

    // set count of surrounding bombs
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {

        let adjacentMines = 0;

        for (const peer of PEERS) {
          if (this.cells[y + peer[0]] && this.cells[y + peer[0]][x + peer[1]] && this.cells[y + peer[0]][x + peer[1]].mine) {
            adjacentMines++;
          }
        }

        this.cells[y][x].surroundedMines = adjacentMines;
      }
    }

    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
        if (this.cells[x][y].mine) {
          this.minesTotal++;
        }
      }
    }


  }

  revealAllMines() {
    for (const row of this.cells) {
      for (const cell of row) {
        if (cell.mine) {
          cell.status = 'clear';
        }
      }
    }
  }

  getRandomInt(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  checkCell(currentCell: Cell): 'gameover' | 'win' | null {

    let result = null;

    console.log(currentCell);

    if (currentCell.mine === true) {
      this.revealAllMines();
      result = 'gameover';
    } else if (currentCell.status !== 'clear') {

      currentCell.status = 'clear';

      if (currentCell.surroundedMines === 0) {
        for (const peer of PEERS) {
          if (
            this.cells[currentCell.row + peer[0]] &&
            this.cells[currentCell.row + peer[0]][currentCell.column + peer[1]]
          ) {
            this.checkCell(this.cells[currentCell.row + peer[0]][currentCell.column + peer[1]]);
          }
        }
      }
    }

    if (result === null) {
      let openCells = 0;

      const size = this.cells.length;

      for (let y = 0; y < size; y++) {
        for (let x = 0; x < size; x++) {
          if (this.cells[y][x].status === 'clear') {
            openCells++;
          }
        }
      }

      console.log('open Cells: ' + openCells + ' size-mines: ' + ((size * size) - this.minesTotal));

      if (openCells >= (size * size) - this.minesTotal) {
        result = 'win';
      }
    }

    return result;
  }
}
