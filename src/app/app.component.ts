import {Component, OnInit} from '@angular/core';
import {Board} from './board/board';
import {Cell} from './board/cell';

@Component({
             selector: 'app-root',
             templateUrl: './app.component.html',
             styleUrls: ['./app.component.css']
           })
export class AppComponent implements OnInit {
  title = 'minesweeper';

  settings: Settings;

  board: Board;

  constructor() {

  }

  ngOnInit(): void {

    if (localStorage.getItem('settings')) {
      this.settings = JSON.parse(localStorage.getItem('settings'));
      console.log(this.settings);
    }

    if (this.settings == null) {
      this.settings = new Settings();
      this.settings.size = 5;
      this.settings.mines = 5;

    }


    this.startNewGame();
  }


  checkCell(cell: Cell) {
    const result = this.board.checkCell(cell);

    if (result === 'gameover') {
      setTimeout(() => {
        alert('Boom');
      }, 100);
    }

    if (result === 'win') {
      setTimeout(() => {
        alert('gewonnen .... Feuerwerk ^_^');
      }, 100);
    }
  }

  markAsFlag(cell: Cell) {

    switch (cell.status) {
      case 'clear':
        break;
      case 'flag':
        cell.status = 'open';
        break;
      case 'open':
        cell.status = 'flag';
    }

  }

  startNewGame() {

    this.board = new Board(this.settings.size, this.settings.mines);
    localStorage.setItem('settings', JSON.stringify(this.settings));
  }
}

export class Settings {
  size: number;
  mines: number;
}
